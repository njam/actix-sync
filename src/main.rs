extern crate actix;

use actix::prelude::*;

fn main() {
    let system = actix::System::new("test");

    let actor1_addr = SyncArbiter::start(1, || MyActor1);

    let actor2_addr = {
        let actor2_factory = move || {
            let actor1_addr = actor1_addr.clone();
            MyActor2 { actor_rcpt: actor1_addr.recipient() }
        };
        SyncArbiter::start(1, actor2_factory)
    };

    actor1_addr.try_send(TextMsg { text: "Hello World".to_string() }).unwrap();
    actor2_addr.try_send(TextMsg { text: "Hello World".to_string() }).unwrap();

    system.run();
}

struct TextMsg {
    text: String
}

impl Message for TextMsg {
    type Result = ();
}

struct MyActor1;

impl Actor for MyActor1 {
    type Context = SyncContext<Self>;
}

impl Handler<TextMsg> for MyActor1 {
    type Result = ();

    fn handle(&mut self, msg: TextMsg, _context: &mut SyncContext<Self>) -> Self::Result {
        println!("Actor1: {}", msg.text);
    }
}

struct MyActor2 {
    actor_rcpt: Recipient<TextMsg>,
}

impl Actor for MyActor2 {
    type Context = SyncContext<Self>;
}

impl Handler<TextMsg> for MyActor2 {
    type Result = ();

    fn handle(&mut self, msg: TextMsg, _context: &mut SyncContext<Self>) -> Self::Result {
        println!("Actor2: {}", msg.text);
    }
}

